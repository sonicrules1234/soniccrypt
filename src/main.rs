use clap::Parser;
use scanpw::scanpw;
use tindercrypt::cryptors::RingCryptor;
use std::path::PathBuf;
use std::io::{Read, Write};
#[derive(Parser)]
#[clap(about = "Encrypts or decrypts files with inputted password")]
struct SonicCryptArgs {
    //#[clap(short, long, help = "Encrypt the specified file.  If not specified, will decrypt")]
    /// Encrypt the specified file.  If not specified, will decrypt
    #[clap(short, long)]
    encrypt: bool,
    
    #[clap(name = "input_file_path", parse(from_os_str))]
    input_file_path: PathBuf,
    
    #[clap(name = "output_file_path", parse(from_os_str))]
    output_file_path: PathBuf,
}

fn main() {
    let args = SonicCryptArgs::parse();
    let should_encrypt = args.encrypt;
    let input_file_path: PathBuf = args.input_file_path;    
    let output_file_path: PathBuf = args.output_file_path;
    let pass = scanpw!("Password: ").trim().to_string();
    let password = pass.as_bytes();
    let cryptor = RingCryptor::new();
    let mut f = std::fs::File::open(input_file_path.clone()).unwrap();
    let mut input_file: Vec<u8> = Vec::new();
    f.read_to_end(&mut input_file).unwrap();
    if should_encrypt {
        //let mut new_file_name = input_file_path.clone().file_name().unwrap().to_os_string();
        //new_file_name.push(".senc");
        let encrypted = cryptor.seal_with_passphrase(password, input_file.as_slice()).unwrap();
        //let mut d = std::fs::File::create(file_path.parent().unwrap().join(new_file_name)).unwrap();
        let mut d = std::fs::File::create(output_file_path).unwrap();
        d.write_all(&encrypted).unwrap();
    } else {        
        let decrypted = cryptor.open(password, input_file.as_slice()).unwrap();
        //let mut d = std::fs::File::create(file_path.parent().unwrap().join(new_file_name)).unwrap();
        let mut d = std::fs::File::create(output_file_path).unwrap();
        d.write_all(&decrypted).unwrap();
    }
    
}
